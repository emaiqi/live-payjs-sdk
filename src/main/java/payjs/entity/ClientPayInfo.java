package payjs.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain=true)
public class ClientPayInfo extends PayInfo{
	/**(收银台参数)用户支付成功后，前端跳转地址*/
	private String callback_url;
	/**(微信收银台)前端表单用的*/
	private String sign;
	/**表单url*/
	private String url_form;
}
